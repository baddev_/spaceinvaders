#include "../headers/player.h"

Player* p_init(){
    Player *p = malloc(sizeof(Player));

    p->sprite = IMG_Load(PLAYER_SPRITE);
    if(p->sprite == NULL){
        printf("Can't load player sprite\n");
        return NULL;
    }

    p->pos.x = SCREEN_WIDTH / 2 - p->sprite->w / 2;
    p->pos.y = SCREEN_HEIGHT - p->sprite->h - 20;
    p->score = 0;
    p->isShooting = 0;

    p->proj = malloc(sizeof(Projectile));

    p->hp.amount = 6;

    // Score display...
    p->score_sprite = malloc(sizeof(SDL_Surface*));
    p->score_pos.x = 5;
    p->score_pos.y = 10;

    p->hp.sprites = malloc(sizeof(SDL_Surface *) * 3);
    p->hp.pos = malloc(sizeof(SDL_Rect) * 3);


    // Player health...
    for(int i = 0; i < 3; i++){
        p->hp.sprites[i] = IMG_Load(HP_FULL);
        if(!p->hp.sprites[i]) printf("Can't load %s\n", HP_FULL);

        if(i == 0)
            p->hp.pos[i].x = SCREEN_WIDTH - (.99 * p->hp.sprites[i]->w);
        else
            p->hp.pos[i].x = p->hp.pos[i - 1].x - p->hp.sprites[i]->w - 5;
        
        p->hp.pos[i].y = 10;
    }

    // p->hp.h0_pos.x = SCREEN_WIDTH - (.99 * p->hp.sprites[0]->w);
    // p->hp.h1_pos.x = p->hp.h0_pos.x - p->hp.sprites[0]->w - 3;
    // p->hp.h2_pos.x = p->hp.h1_pos.x - p->hp.sprites[0]->w - 3;
    
    // p->hp.h0_pos.y = 10;
    // p->hp.h1_pos.y = 10;
    // p->hp.h2_pos.y = 10;

    // printf("%d; %d", p->hp.h0_pos.x, p->hp.h0_pos.y);
    return p;
}

void p_left(Player* p){
    if((p->pos.x - MX) > 0)
        p->pos.x -= MX;
}

void p_right(Player* p){
    if(p->pos.x + MX < SCREEN_WIDTH)
        p->pos.x += MX;
}

void p_shoot(Player* p, Enemy **e, Boss *b, int *e_remaining){ // , Enemy** e){
    if(p->isShooting == 0){
        //printf("Initializing...\n");
        p->isShooting = 1;
        p->proj->sprite = IMG_Load(MISSILE_SPRITE);

        if(p->proj->sprite == NULL){
            printf("Can't load missile sprite\n");
            exit(-1);
        }

        p->proj->pos.x = p->pos.x + 8;
        p->proj->pos.y = p->pos.y - 12;
    }
    else{
        p->proj->pos.y -= MY;
        //printf("Keep going\n");

        //If enemy is hit
        int px = p->proj->pos.x, py = p->proj->pos.y; // Projectile coords
        int pw = p->proj->sprite->w, ph = p->proj->sprite->h; // Projectile size

        //if proj x is between enemy x and enemy x + enemy width (and same for y and height)
        for(int i = 0; i < NB_ENEMIES; i++){
            if (px >= e[i]->pos.x && px <= e[i]->pos.x + ENEMY_WIDTH){
                if (py >= e[i]->pos.y && py <= e[i]->pos.y + ENEMY_HEIGHT && e[i]->isHitable == 1){
                    // printf("ENEMY HIT!\n");
                    e_destroy(e[i], e_remaining);
                    p->isShooting = 0;
                    p->score += 10;
                }
            }
        }

        if(b->isHitable == 1){
            if(px + pw >= b->pos.x && px <= b->pos.x + b->sprite->w){
                if(py <= b->pos.y + b->sprite->h && py + ph <= b->pos.y){
                    b->hp--;
                    p->isShooting = 0;
                    
                    if(b->hp <= 0){
                        p->score += 100;
                        b->isHitable = 0;
                    }

                }
            }
        }

        if(py <= 0){
            p->isShooting = 0;
            //printf("Projectile Destroyed\n");
        }
    }
        
}

int p_isDead(Player *p, Enemy **e){

    // We check if the height of last row enemy > player height
    for(int i = NB_ENEMIES - 1; i >= 0; i--){
        if(e[i]->sprite != NULL){
            if(e[i]->sprite->h + e[i]->pos.y >= p->pos.y){
                return 1;
            }
        }
    }

    if (p->hp.amount <= 0)
        return 1;

    return 0;
}

void p_blit(Player *p, Enemy **e, Boss *b, SDL_Surface *app_window, TTF_Font *font, int *e_remaining){

    // If score is 0, we want it to show as 00000
    // So we setup a score string

    char s[24];
    SDL_Color white = {255, 255, 255}, black = {0, 0, 0};
    if(p->score < 10) sprintf(s, "SCORE:  0000%d", p->score);
    else if(p->score < 100) sprintf(s, "SCORE:  000%d", p->score);
    else if(p->score < 1000) sprintf(s, "SCORE:  00%d", p->score);
    else if(p->score < 10000) sprintf(s, "SCORE:  0%d", p->score);
    else if(p->score < 100000) sprintf(s, "SCORE:  %d", p->score);

    if(p->isShooting == 1){
        p_shoot(p, e, b, e_remaining);
        SDL_BlitSurface(p->proj->sprite, NULL, app_window, &p->proj->pos);
    }
    

    // This block should go in e_shoot()
    if(p->hp.amount > 4){
        p->hp.amount % 2 ? p->hp.sprites[2] = IMG_Load(HP_HALF) : IMG_Load(HP_FULL);
    }
    else{
        p->hp.sprites[2] = IMG_Load(HP_EMPTY);
        if(p->hp.amount > 2){
            p->hp.amount % 2 ? p->hp.sprites[1] = IMG_Load(HP_HALF) : IMG_Load(HP_FULL);
        }
        else{
            p->hp.sprites[1] = IMG_Load(HP_EMPTY);
            p->hp.amount % 2 ? p->hp.sprites[0] = IMG_Load(HP_HALF) : IMG_Load(HP_FULL);
        }
    }

    // SDL_BlitSurface(p->hp.full, NULL, app_window, &p->hp.h0_pos);
    // SDL_BlitSurface(p->hp.half, NULL, app_window, &p->hp.h1_pos);
    // SDL_BlitSurface(p->hp.empty, NULL, app_window, &p->hp.h2_pos);

    for(int i = 0; i < 3; i++){
        SDL_BlitSurface(p->hp.sprites[i], NULL, app_window, &p->hp.pos[i]);
    }
        
    // sprintf(s, "SCORE:  %d", p->score);
    p->score_sprite = TTF_RenderText_Shaded(font, s, white, black);
    SDL_BlitSurface(p->score_sprite, NULL, app_window, &p->score_pos);

    SDL_BlitSurface(p->sprite, NULL, app_window, &p->pos);
}

void p_free(Player* p){
    for(int i = 0; i < 3; i++){
        free(p->hp.sprites[i]);
    }
    free(p->hp.pos);
    free(p->sprite);
    free(p->score_sprite);
    free(p->proj);
    free(p);
}