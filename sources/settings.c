#include "../headers/settings.h"

int init(){
    // Loading SDL...
    if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0){  // If failed ->
        fprintf(stderr, "Une erreur est survenue: %s\n", SDL_GetError());  // Printing error
        return -1;  // Quit
    }

    // Loading TTF...
    if(TTF_Init() == -1){
        fprintf(stderr, "Une erreur est survenue: %s\n", TTF_GetError());
        return -1;
    }

    // Loading Mixer...
    if(Mix_Init(MIX_INIT_MP3|MIX_INIT_MOD) == -1){
        fprintf(stderr, "Une erreur est survenue: %s\n", Mix_GetError());
        return -1;
    }
    if(Mix_OpenAudio(22050, AUDIO_S16SYS, 2, 4096) != 0)
        printf("%s\n", Mix_GetError());
    // Mix_Init(MIX_INIT_OGG | MIX_INIT_MOD);

    return 1;
}

// Boolean
int mouseHover(int mx, int my, int ow, int oh, SDL_Rect objectCoords){
    int ox = objectCoords.x, oy = objectCoords.y;
    
    /**
     * Mouse is hovering if:
     * ox <= mx <= ox + ow AND
     * oy <= my <= oy + oh 
     */
    if (ox <= mx && mx <= ox + ow){
        if (oy <= my && my <= oy + oh){
            return 1;
        }
        else{
            return 0;
        }
    }
    else{
        return 0;
    }
}

// Center a text between the two arrows
// in settings screen
int centerText(int text_w, int ra_w){
    int width = LEFT_ARROW_X + RIGHT_ARROW_X + ra_w;
    return (width / 2) - (text_w / 2);
}

void settings_init(Settings *s){
    if(!s || s->fs_current == 0){
        FILE *file = fopen(CONFIG_FILE, "r");
        if(!file){
            printf("No file\n");
            int fs_current = 0;
            int audio_current = 1;
            int difficulty_current = 1;
        }else{
            // Reading config file
            fscanf(file, "%d\n", &s->fs_current);
            fscanf(file, "%d\n", &s->audio_current);
            fscanf(file, "%d",   &s->difficulty_current);

            // Checking file input
            if(s->fs_current < 0 || s->fs_current > 1)
                s->fs_current = 0;
            if(s->audio_current < 0 || s->audio_current > 1)
                s->audio_current = 0;
            if(s->difficulty_current < 1 || s->difficulty_current > 3)
                s->difficulty_current = 1;
            fclose(file);
        }

    }
}

void settings_show(SDL_Surface *app_window, Settings *settings){

    SDL_Color white = {255, 255, 255}, green = {0, 255, 0}, black = {0, 0, 0};

    SDL_Surface *full_screen_label, *full_screen_value;
    SDL_Surface *fs_left_arrow, *fs_right_arrow;
    
    SDL_Surface *audio_label, *audio_value;
    SDL_Surface *audio_left_arrow, *audio_right_arrow;

    SDL_Surface *diff_label, *diff_value;
    SDL_Surface *diff_right_arrow, *diff_left_arrow;

    TTF_Font *font = TTF_OpenFont(MAIN_POLICE, 16);
    TTF_Font *arrow_font = TTF_OpenFont(SECONDARY_POLICE, 16);
    full_screen_label = TTF_RenderText_Shaded(font, "FULL SCREEN", white, black);
    full_screen_value = TTF_RenderText_Shaded(font, settings->fs_current == 1 ? "ENABLED" : "DISABLED", white, black);

    audio_label = TTF_RenderText_Shaded(font, "SOUNDS", white, black);
    audio_value = TTF_RenderText_Shaded(font, settings->audio_current == 1 ? "ENABLED" : "DISABLED", white, black);

    diff_label = TTF_RenderText_Shaded(font, "DIFFICULTY", white, black);
    switch(settings->difficulty_current){
        case 1:
            diff_value = TTF_RenderText_Shaded(font, "EASY", white, black);
            break;

        case 2:
            diff_value = TTF_RenderText_Shaded(font, "MEDIUM", white, black);
            break;

        case 3:
            diff_value = TTF_RenderText_Shaded(font, "HARD", white, black);
            break;
    }

    fs_left_arrow = TTF_RenderText_Shaded(arrow_font, LEFT_ARROW, white, black);
    fs_right_arrow = TTF_RenderText_Shaded(arrow_font, RIGHT_ARROW, white, black);

    audio_left_arrow = TTF_RenderText_Shaded(arrow_font, LEFT_ARROW, white, black);
    audio_right_arrow = TTF_RenderText_Shaded(arrow_font, RIGHT_ARROW, white, black);

    diff_left_arrow = TTF_RenderText_Shaded(arrow_font, LEFT_ARROW, white, black);
    diff_right_arrow = TTF_RenderText_Shaded(arrow_font, RIGHT_ARROW, white, black);

    SDL_Rect fs_label_pos, fs_value_pos;
    SDL_Rect fs_left_arrow_pos, fs_right_arrow_pos;

    SDL_Rect audio_label_pos, audio_value_pos;
    SDL_Rect audio_left_arrow_pos, audio_right_arrow_pos;

    SDL_Rect diff_label_pos, diff_value_pos;
    SDL_Rect diff_left_arrow_pos, diff_right_arrow_pos;

    // Full screen layout
    fs_label_pos.x = 50;
    fs_label_pos.y = 60;

    fs_left_arrow_pos.x = LEFT_ARROW_X;
    fs_left_arrow_pos.y = 60;

    fs_right_arrow_pos.x = RIGHT_ARROW_X;
    fs_right_arrow_pos.y = 60;

    fs_value_pos.x = centerText(full_screen_value->w, fs_right_arrow->w);
    fs_value_pos.y = 60;

    // Audio Layout
    audio_label_pos.x = 50;
    audio_label_pos.y = 90;

    audio_left_arrow_pos.x = LEFT_ARROW_X;
    audio_left_arrow_pos.y = 90;

    audio_right_arrow_pos.x = RIGHT_ARROW_X;
    audio_right_arrow_pos.y = 90;

    audio_value_pos.x = centerText(audio_value->w, audio_right_arrow->w);
    audio_value_pos.y = 90;

    // Difficulty Layout
    diff_label_pos.x = 50;
    diff_label_pos.y = 120;

    diff_left_arrow_pos.x = LEFT_ARROW_X;
    diff_left_arrow_pos.y = 120;

    diff_right_arrow_pos.x = RIGHT_ARROW_X;
    diff_right_arrow_pos.y = 120;

    diff_value_pos.x = centerText(diff_value->w, diff_right_arrow->w);
    diff_value_pos.y = 120;

    int loop = 1;
    SDL_Event event;
    while(loop){
        SDL_WaitEvent(&event);
        switch(event.type){
            case SDL_KEYDOWN:
                switch(event.key.keysym.sym){
                    case SDLK_ESCAPE:
                        loop = 0;
                        break;
                }
                break;
            
            case SDL_MOUSEMOTION:

                // Full screen arrows...
                if(mouseHover(event.button.x, event.button.y, fs_left_arrow->w, fs_left_arrow->h, fs_left_arrow_pos))
                    fs_left_arrow = TTF_RenderText_Shaded(arrow_font, LEFT_ARROW, green, black);
                else
                    fs_left_arrow = TTF_RenderText_Shaded(arrow_font, LEFT_ARROW, white, black);

                if(mouseHover(event.button.x, event.button.y, fs_right_arrow->w, fs_right_arrow->h, fs_right_arrow_pos))
                    fs_right_arrow = TTF_RenderText_Shaded(arrow_font, RIGHT_ARROW, green, black);
                else
                    fs_right_arrow = TTF_RenderText_Shaded(arrow_font, RIGHT_ARROW, white, black);

                // Audio Arrows...
                if(mouseHover(event.button.x, event.button.y, audio_right_arrow->w, audio_right_arrow->h, audio_right_arrow_pos))
                    audio_right_arrow = TTF_RenderText_Shaded(arrow_font, RIGHT_ARROW, green, black);
                else
                    audio_right_arrow = TTF_RenderText_Shaded(arrow_font, RIGHT_ARROW, white, black);

                if(mouseHover(event.button.x, event.button.y, audio_left_arrow->w, audio_left_arrow->h, audio_left_arrow_pos))
                    audio_left_arrow = TTF_RenderText_Shaded(arrow_font, LEFT_ARROW, green, black);
                else
                    audio_left_arrow = TTF_RenderText_Shaded(arrow_font, LEFT_ARROW, white, black);

                if (mouseHover(event.button.x, event.button.y, diff_left_arrow->w, diff_left_arrow->h, diff_left_arrow_pos))
                    diff_left_arrow = TTF_RenderText_Shaded(arrow_font, LEFT_ARROW, green, black);
                else
                    diff_left_arrow = TTF_RenderText_Shaded(arrow_font, LEFT_ARROW, white, black);

                if (mouseHover(event.button.x, event.button.y, diff_right_arrow->w, diff_right_arrow->h, diff_right_arrow_pos))
                    diff_right_arrow = TTF_RenderText_Shaded(arrow_font, RIGHT_ARROW, green, black);
                else
                    diff_right_arrow = TTF_RenderText_Shaded(arrow_font, RIGHT_ARROW, white, black);

                break;

            case SDL_MOUSEBUTTONDOWN:
                if(event.button.button == SDL_BUTTON_LEFT){

                    // Full screen arrows
                    if (mouseHover(event.button.x, event.button.y, fs_left_arrow->w, fs_left_arrow->h, fs_left_arrow_pos)){
                        settings->fs_current = settings->fs_current == 1 ? 0 : 1;
                        full_screen_value = TTF_RenderText_Shaded(font, settings->fs_current == 1 ? "ENABLED" : "DISABLED", white, black);
                    }

                    if(mouseHover(event.button.x, event.button.y, fs_right_arrow->w, fs_right_arrow->h, fs_right_arrow_pos)){
                        settings->fs_current = settings->fs_current == 1 ? 0 : 1;
                        full_screen_value = TTF_RenderText_Shaded(font, settings->fs_current == 1 ? "ENABLED" : "DISABLED", white, black);
                    }

                    // Audio arrows
                    // if(mouseHover(event.button.x, event.button.y, audio_right_arrow->w, audio_right_arrow->h, audio_right_arrow_pos)){
                    //     settings->audio_current = settings->audio_current == 1 ? 0 : 1;
                    //     audio_value = TTF_RenderText_Shaded(font, settings->audio_current == 1 ? "ENABLED" : "DISABLED", white, black);
                    // }

                    // if(mouseHover(event.button.x, event.button.y, audio_left_arrow->w, audio_left_arrow->h, audio_left_arrow_pos)){
                    //     settings->audio_current = settings->audio_current == 1 ? 0 : 1;
                    //     audio_value = TTF_RenderText_Shaded(font, settings->audio_current == 1 ? "ENABLED" : "DISABLED", white, black);
                    // }

                    // Because difficulty is not boolean type its more difficult to handle it
                    if (mouseHover(event.button.x, event.button.y, diff_left_arrow->w, diff_left_arrow->h, diff_left_arrow_pos)){
                        settings->difficulty_current--;
                        if(settings->difficulty_current < 1) settings->difficulty_current = 3;
                        switch(settings->difficulty_current){
                            case 1:
                                diff_value = TTF_RenderText_Shaded(font, "EASY", white, black);
                                // diff_value_pos.x = centerText(diff_value->w, diff_right_arrow->w);
                                break;
                            
                            case 2:
                                diff_value = TTF_RenderText_Shaded(font, "MEDIUM", white, black);
                                // diff_value_pos.x = centerText(diff_value->w, diff_right_arrow->w);
                                break;
                            
                            case 3:
                                diff_value = TTF_RenderText_Shaded(font, "HARD", white, black);
                                // diff_value_pos.x = centerText(diff_value->w, diff_right_arrow->w);
                                break;
                        }

                        diff_value_pos.x = centerText(diff_value->w, diff_right_arrow->w);
                    }

                    if (mouseHover(event.button.x, event.button.y, diff_right_arrow->w, diff_right_arrow->h, diff_right_arrow_pos)){
                        settings->difficulty_current++;
                        if(settings->difficulty_current > 3) settings->difficulty_current = 1;
                        switch (settings->difficulty_current){
                            case 1:
                                diff_value = TTF_RenderText_Shaded(font, "EASY", white, black);
                                diff_value_pos.x = centerText(diff_value->w, diff_right_arrow->w);
                                break;

                            case 2:
                                diff_value = TTF_RenderText_Shaded(font, "MEDIUM", white, black);
                                diff_value_pos.x = centerText(diff_value->w, diff_right_arrow->w);
                                break;

                            case 3:
                                diff_value = TTF_RenderText_Shaded(font, "HARD", white, black);
                                diff_value_pos.x = centerText(diff_value->w, diff_right_arrow->w);
                                break;
                        }
                    }
                    diff_value_pos.x = centerText(diff_value->w, diff_right_arrow->w);
                }
                break;
        }
        if(settings->fs_current == 1){
            app_window = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 32, SDL_HWSURFACE | SDL_FULLSCREEN);
        }else{
            app_window = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 32, SDL_HWSURFACE);
        }

        // if(settings->audio_current == 0){
        //     Mix_Volume(2, 0);
        // }else{
        //     Mix_Volume(2, MIX_MAX_VOLUME);
        // }

        // if(settings->audio_current == 1)
        //     Mix_Pause(-1);
        // else
        //     Mix_Resume(-1);

        SDL_FillRect(app_window, NULL, SDL_MapRGB(app_window->format, 0, 0, 0));
        SDL_BlitSurface(full_screen_label, NULL, app_window, &fs_label_pos);
        SDL_BlitSurface(full_screen_value, NULL, app_window, &fs_value_pos);
        SDL_BlitSurface(fs_left_arrow, NULL, app_window, &fs_left_arrow_pos);
        SDL_BlitSurface(fs_right_arrow, NULL, app_window, &fs_right_arrow_pos);

        // SDL_BlitSurface(audio_label, NULL, app_window, &audio_label_pos);
        // SDL_BlitSurface(audio_value, NULL, app_window, &audio_value_pos);
        // SDL_BlitSurface(audio_left_arrow, NULL, app_window, &audio_left_arrow_pos);
        // SDL_BlitSurface(audio_right_arrow, NULL, app_window, &audio_right_arrow_pos);

        SDL_BlitSurface(diff_label, NULL, app_window, &diff_label_pos);
        SDL_BlitSurface(diff_value, NULL, app_window, &diff_value_pos);
        SDL_BlitSurface(diff_left_arrow, NULL, app_window, &diff_left_arrow_pos);
        SDL_BlitSurface(diff_right_arrow, NULL, app_window, &diff_right_arrow_pos);

        SDL_Flip(app_window);
    }

    free(full_screen_label);
    free(full_screen_value);
    free(fs_left_arrow);
    free(fs_right_arrow);

    free(audio_label);
    free(audio_value);
    free(audio_left_arrow);
    free(audio_right_arrow);

    free(diff_label);
    free(diff_value);
    free(diff_left_arrow);
    free(diff_right_arrow);

    free(font);
    free(arrow_font);

    settings_save(*settings);
}

void settings_save(Settings s){
    FILE *file = fopen(CONFIG_FILE, "w");
    if(!file){
        printf("Can't open %s\n", CONFIG_FILE);
        // return s;
    }else{
        fprintf(file, "%d\n", s.fs_current);
        fprintf(file, "%d\n", s.audio_current);
        fprintf(file, "%d",   s.difficulty_current);
        // printf("Wrote %d\n", s.difficulty_current);
        fclose(file);
    }
    // return s;
}