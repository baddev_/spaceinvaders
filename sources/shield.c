#include "../headers/shield.h"

Shield** s_init(){
    Shield **shields = malloc(sizeof(Shield *) * NB_SHIELDS);
    for(int i = 0; i < NB_SHIELDS; i++){
        shields[i] = malloc(sizeof(Shield));
        shields[i]->sprite = malloc(sizeof(SDL_Surface));
        shields[i]->sprite = IMG_Load(SHIELD_FULL);
        shields[i]->pos.x = ((i + 1) * 50) + 60 * i;
        shields[i]->pos.y = SCREEN_HEIGHT - 100;
        shields[i]->hp = 8;
    }

    return shields;
}

void s_blit(Shield **s, SDL_Surface *app_window){
    for(int i = 0; i < NB_SHIELDS; i++){
        // Do not blit if destroyed
        if(s[i]->hp > 0){
            switch(s[i]->hp){                
                case 4:
                    s[i]->sprite = IMG_Load(SHIELD_HALF);
                    break;

                default:
                    break;
            }
            SDL_BlitSurface(s[i]->sprite, NULL, app_window, &s[i]->pos);
        }
        else{
            s[i]->sprite = NULL;
        }
    }
}

void s_free(Shield **s){
    for(int i = 0; i < NB_SHIELDS; i++){
        free(s[i]->sprite);
        free(s[i]);
    }
}