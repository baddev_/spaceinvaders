#include <time.h>
#include <stdio.h>
#include <stdlib.h>

#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_audio.h>
#include <SDL/SDL_mixer.h>

#include "../headers/settings.h"
#include "../headers/game.h"

int main(){

    if(init() != 1) return -1;
    Settings *settings = malloc(sizeof(Settings));
    settings_init(settings);

    // Creating sounds
    Mix_Music *music = Mix_LoadMUS(THEME_SOUND);
    if(music == NULL) printf("NO MUSIC: %s\n", Mix_GetError());


    // Create the window
    SDL_Surface *app_window, *title, *playBtn, *quitBtn, *optBtn;
    if(settings->fs_current == 0)
        app_window = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 32, SDL_HWSURFACE);
    else
        app_window = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 32, SDL_HWSURFACE | SDL_FULLSCREEN);
    if(app_window == NULL){  // If failed->
        fprintf(stderr, "Une erreur est survenue: %s\n", SDL_GetError());  // Printing Error
        return -1;  // Quit
    }

    // Creating colors...
    SDL_Color black = {0, 0, 0}, white = {255, 255, 255}, mainColor = {0, 255, 0};

    // Loading polices...
    TTF_Font *mainPolice = TTF_OpenFont(MAIN_POLICE, 30), *btnPolice = TTF_OpenFont(MAIN_POLICE, 20);
    title   = TTF_RenderText_Shaded(mainPolice, TITLE, mainColor, black);
    playBtn = TTF_RenderText_Shaded(btnPolice, PLAY_BTN, white, black);
    quitBtn = TTF_RenderText_Shaded(btnPolice, QUIT_BTN, white, black);
    optBtn  = TTF_RenderText_Shaded(btnPolice, OPT_BTN, white, black);

    SDL_Rect titlePos, playBtnPos, quitBtnPos, optBtnPos;
    titlePos.x = SCREEN_WIDTH / 2 - (title->w / 2);
    titlePos.y = 10;

    playBtnPos.x = SCREEN_WIDTH / 2 - playBtn->w / 2; // SCREEN_WIDTH - 130 - playBtn->w;
    playBtnPos.y = SCREEN_HEIGHT - 150 - playBtn->h;

    optBtnPos.x = SCREEN_WIDTH / 2 - optBtn->w / 2; // - 100 - quitBtn->w;
    optBtnPos.y = SCREEN_HEIGHT - 100 - quitBtn->h;

    quitBtnPos.x = SCREEN_WIDTH / 2 - quitBtn->w / 2; // - 70 - quitBtn->w;
    quitBtnPos.y = SCREEN_HEIGHT - 50 - playBtn->h;


    SDL_Event e;
    int loop = 1, error_code = 0;
    if(Mix_PlayMusic(music, -1) == -1)
        printf("%s\n", Mix_GetError());
    while (loop){
        
        SDL_WaitEvent(&e);
        switch (e.type){
            case SDL_QUIT:
                loop = 0;
                break;

            case SDL_KEYDOWN: //Keyboard events handling...
                switch (e.key.keysym.sym){
                    case SDLK_ESCAPE:
                        loop = 0;
                        break;
                }
                break; // End of Keyboard events handling...
            
            case SDL_MOUSEMOTION: //Mouse motion events handling

                // Checking if mouse hover play btn
                if(mouseHover(e.button.x, e.button.y, playBtn->w, playBtn->h, playBtnPos) == 1){
                    playBtn = TTF_RenderText_Shaded(btnPolice, PLAY_BTN, mainColor, black);
                }else{
                    playBtn = TTF_RenderText_Shaded(btnPolice, PLAY_BTN, white, black);
                }

                //Checking for quitBtn
                if(mouseHover(e.button.x, e.button.y, quitBtn->w, quitBtn->h, quitBtnPos) == 1){
                    quitBtn = TTF_RenderText_Shaded(btnPolice, QUIT_BTN, mainColor, black);
                }else{
                    quitBtn = TTF_RenderText_Shaded(btnPolice, QUIT_BTN, white, black);
                }

                //Checking for optBtn
                if(mouseHover(e.button.x, e.button.y, optBtn->w, optBtn->h, optBtnPos) == 1){
                    optBtn = TTF_RenderText_Shaded(btnPolice, OPT_BTN, mainColor, black);
                }else{
                    optBtn = TTF_RenderText_Shaded(btnPolice, OPT_BTN, white, black);
                }
            break; // End of mouse motion events...

            case SDL_MOUSEBUTTONDOWN:
                if(e.button.button == SDL_BUTTON_LEFT){
                    if(mouseHover(e.button.x, e.button.y, playBtn->w, playBtn->h, playBtnPos))
                        // if(game_start != 1) loop = 0;
                        if(process(app_window, settings) != 0) loop = 0;
                    
                    if(mouseHover(e.button.x, e.button.y, quitBtn->w, quitBtn->h, quitBtnPos)){
                       loop = 0;
                    }

                    if(mouseHover(e.button.x, e.button.y, optBtn->w, optBtn->h, optBtnPos))
                        settings_show(app_window, settings);
                }
            break;
        }

        // End of Switch...

        SDL_FillRect(app_window, NULL, SDL_MapRGB(app_window->format, 0, 0, 0));
        SDL_BlitSurface(title, NULL, app_window, &titlePos);
        SDL_BlitSurface(playBtn, NULL, app_window, &playBtnPos);
        SDL_BlitSurface(quitBtn, NULL, app_window, &quitBtnPos);
        SDL_BlitSurface(optBtn, NULL, app_window, &optBtnPos);
        SDL_Flip(app_window);

    } // End of main loop

    free(settings);
    SDL_FreeSurface(app_window);
    SDL_FreeSurface(title);
    SDL_FreeSurface(playBtn);
    SDL_FreeSurface(quitBtn);
    SDL_FreeSurface(optBtn);
    Mix_CloseAudio();
    TTF_CloseFont(mainPolice);

    Mix_Quit();
    TTF_Quit();
    SDL_Quit();
    return 0;
}