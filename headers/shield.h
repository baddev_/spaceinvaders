#ifndef SHIELD_H_
#define SHIELD_H_

    #include <SDL/SDL.h>
    #include <SDL/SDL_image.h>
    #include "settings.h"

    typedef struct shield{
        SDL_Surface *sprite;
        SDL_Rect pos;
        int hp;
    }Shield;

    Shield** s_init();

    void s_blit(Shield **s, SDL_Surface *app_window);
    void s_free(Shield **s);

    

#endif