#ifndef PROJECTILE_H_
#define PROJECTILE_H_

    #include <SDL/SDL_image.h>
    #include <SDL/SDL_audio.h>

typedef struct projectile{
        SDL_Rect pos;
        SDL_Surface* sprite;
}Projectile;

#endif