/**
 * ENEMY CLASS RELATED CODE
 *
 * void randomEnemyGenerator(Enemy *e, int i, int j)
 * Enemy** init_enemies(int nb)
 * void e_move(Enemy* e)
 * void e_destroy(Enemy* e)
 */

#ifndef ENEMY_H_
#define ENEMY_H_

    #include <time.h>
    #include <stdio.h>
    #include <stdlib.h>
    #include <SDL/SDL.h>
    #include <SDL/SDL_image.h>

    #include "settings.h"
    #include "projectile.h"
    #include "shield.h"

    typedef struct enemy{
        int direction; // 1 || -1, Right or left
        int move_count; // Used to slow down enemies (bc of pollevent)

        int isHitable; // Is enemy still alive (rename this later)

        int isShooting;
        Projectile *proj;

        SDL_Rect pos;
        SDL_Surface *sprite;
    }Enemy;

    // These two work together
    Enemy** init_enemies();
    void randomEnemyGenerator(Enemy *e, int i, int j);

    // Draw all enemies on window
    void e_draw(Enemy **e, SDL_Surface *app_window, int nb);

    void e_move(Enemy **e, int nb);
    void e_free(Enemy **e, int nb);
    void e_down(Enemy **e, int nb);

    void e_shoot(Enemy **e, int difficuly, Shield** s);
    int e_count(Enemy **e);

    void e_destroy(Enemy* e, int *e_remaining);
#endif