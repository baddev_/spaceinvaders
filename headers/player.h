/**
 * PLAYER CLASS & CORE RELATED CODE
 * 
 * Player* p_init()
 * void p_left(Player* p)
 * void p_right(Player* p)
 * void p_shoot(Player* p, Enemy** e)
 * void p_free(Player* p)
 * 
 * Enemy: 8x8
 * Player: 16x7
 * */

#ifndef PLAYER_H_
#define PLAYER_H_

    #include <stdio.h>
    #include <stdlib.h>
    #include <SDL/SDL.h>
    #include <SDL/SDL_image.h>

    #include "enemy.h"
    #include "settings.h"
    #include "projectile.h"
    #include "boss.h"


    typedef struct hp{
        int amount;

        SDL_Surface **sprites;
        SDL_Rect  *pos;
    }Hp;

    typedef struct player{
        SDL_Surface *sprite;
        SDL_Surface *score_sprite;
        SDL_Rect pos;
        SDL_Rect score_pos;

        Hp hp;
        int score;
        int isShooting;
        // int shoot_cout;
        Projectile *proj;
    }Player;

    Player* p_init();

    void p_left(Player *p);
    void p_right(Player *p);

    void p_shoot(Player *p, Enemy **e, Boss *b, int *e_remaining); // , Enemy** e);
    int p_isDead(Player *p, Enemy **e); // boolean
    void p_blit(Player *p, Enemy **e, Boss *b, SDL_Surface *app_window, TTF_Font *font, int *e_remaining);

    void p_free(Player *p);

#endif