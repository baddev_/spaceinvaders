#ifndef BOSS_H_
#define BOSS_H_

#include <time.h>
    #include <stdio.h>
    #include <stdlib.h>
    #include <SDL/SDL.h>
    #include <SDL/SDL_image.h>

    #include "settings.h"
    #include "projectile.h"
    #include "shield.h"

    typedef struct boss{
        int direction;
        int move_count;
        int hp;

        int isHitable;
        int hasSpawn;

        int isShooting;
        int mx;
        int my; // Projectile speed

        Projectile *proj;
        SDL_Rect pos;
        SDL_Surface *sprite;
    }Boss;

    Boss* b_init();

    void b_draw(Boss *b, SDL_Surface *app_window);
    void b_move(Boss *b);
    void b_free(Boss *b);
    void b_shoot(Boss *b, int diff, Shield **s);
    void b_destroy(Boss *b);

#endif
